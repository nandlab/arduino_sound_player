#include <DFPlayerMini.h>

static int mymain(void) {
  static const int pinStart = 5;
  static const int pinStop = 6;
  static const int pinVolumeDecrease = 7;
  static const int pinVolumeIncrease = 8;
  
  static const int pinBusy = 4;
  static const int pinReceive = 3;
  static const int pinTransmit = 2;
  
  static const int soundFileNum = 1;
  
  static const int volumeMaxLevel = 5;
  static const int volumeMin = 0;
  static const int volumeMax = 30;

  int volumeLevel = 3;

  Serial.begin(9600);
  pinMode(pinStart, INPUT_PULLUP);
  pinMode(pinStop, INPUT_PULLUP);
  pinMode(pinVolumeDecrease, INPUT_PULLUP);
  pinMode(pinVolumeIncrease, INPUT_PULLUP);
  bool pinStartPrevValue = digitalRead(pinStart);
  bool pinStopPrevValue = digitalRead(pinStop);
  bool pinVolumeDecreasePrevValue = digitalRead(pinVolumeDecrease);
  bool pinVolumeIncreasePrevValue = digitalRead(pinVolumeIncrease);
  
  Serial.print("Max volume level is ");
  Serial.println(volumeMaxLevel);
  Serial.print("Default volume level is ");
  Serial.println(volumeLevel);
  Serial.flush();

  Serial.println("DFPlayerMini: constructing...");
  Serial.flush();
  DFPlayerMini dfplayer;
  Serial.println("DFPlayerMini: initializing...");
  Serial.flush();
  dfplayer.init(pinBusy, pinReceive, pinTransmit, NULL);
  Serial.println("DFPlayerMini: set default volume level...");
  Serial.flush();
  dfplayer.setVolume(volumeLevel * volumeMax / volumeMaxLevel);
  Serial.println("DFPlayerMini: ready");
  Serial.flush();
  delay(10);
  bool pinVal;
  while (1) {
    pinVal = digitalRead(pinStart);
    if (!pinVal && pinStartPrevValue) {
      Serial.println("DFPlayerMini: start");
      dfplayer.playFile(soundFileNum);
    }
    pinStartPrevValue = pinVal;
    
    pinVal = digitalRead(pinStop);
    if (!pinVal && pinStopPrevValue) {
      Serial.println("DFPlayerMini: stop");
      dfplayer.stop();
    }
    pinStopPrevValue = pinVal;
    
    pinVal = digitalRead(pinVolumeDecrease);
    if (!pinVal && pinVolumeDecreasePrevValue) {
      if (volumeLevel) {
        volumeLevel--;
        Serial.print("DFPlayerMini: decrease volume to level ");
        Serial.println(volumeLevel);
        Serial.flush();
        dfplayer.setVolume(volumeLevel * volumeMax / volumeMaxLevel);
      }
      else {
        Serial.println("DFPlayerMini: Already minimum volume");
      }
    }
    pinVolumeDecreasePrevValue = pinVal;
    
    pinVal = digitalRead(pinVolumeIncrease);
    if (!pinVal && pinVolumeIncreasePrevValue) {
      if (volumeLevel < volumeMaxLevel) {
        volumeLevel++;
        Serial.print("DFPlayerMini: increase volume to level ");
        Serial.println(volumeLevel);
        Serial.flush();
        dfplayer.setVolume(volumeLevel * volumeMax / volumeMaxLevel);
      }
      else {
        Serial.println("DFPlayerMini: Already maximum volume");        
      }
    }
    pinVolumeIncreasePrevValue = pinVal;
    
    delay(25);
  }
}

void setup() {
  // put your setup code here, to run once:
  mymain();
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Program completed");
  Serial.flush();
  delay(10000);
}
